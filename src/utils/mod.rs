use rustc::hir::*;
use rustc::lint::*;
use rustc::ty;
use syntax::ast;
use syntax::codemap::Span;
use syntax::ptr::P;

pub mod cargo;

pub type MethodArgs = HirVec<P<Expr>>;

/// Returns true if this `expn_info` was expanded by any macro.
pub fn in_macro<T: LintContext>(cx: &T, span: Span) -> bool {
    cx.sess().codemap().with_expn_info(span.expn_id, |info| info.is_some())
}

#[derive(PartialEq)]
pub enum MethodLateContext {
    TraitDefaultImpl,
    TraitImpl,
    PlainImpl,
}

pub fn method_context(cx: &LateContext, id: ast::NodeId, span: Span) -> MethodLateContext {
    let def_id = cx.tcx.map.local_def_id(id);
    match cx.tcx.associated_items.borrow().get(&def_id) {
        None => span_bug!(span, "missing method descriptor?!"),
        Some(item) => {
            match item.container {
                ty::TraitContainer(..) => MethodLateContext::TraitDefaultImpl,
                ty::ImplContainer(cid) => {
                    match cx.tcx.impl_trait_ref(cid) {
                        Some(_) => MethodLateContext::TraitImpl,
                        None => MethodLateContext::PlainImpl,
                    }
                }
            }
        }
    }
}
