#![feature(rustc_private)]

#[macro_use]
extern crate syntax;
#[macro_use]
extern crate rustc;

#[macro_use]
extern crate matches as matches_macro;

extern crate rustc_serialize;
extern crate rustc_plugin;

pub mod doc_coverage;
#[macro_use]
pub mod utils;

pub fn register_plugins(reg: &mut rustc_plugin::Registry) {
    reg.register_late_lint_pass(Box::new(doc_coverage::DocCoverage::new()));
}