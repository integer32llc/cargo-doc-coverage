# cargo doc-coverage

This crate prints out the percentage of public items that have documentation.

## Installation and usage

I have gotten this to work with `rustc 1.15.0-nightly (d9bdc636d 2016-11-24)`.
Assuming you use [rustup][], get on that nightly and install this plugin with
the following commands:

```
$ rustup toolchain install nightly-2016-11-25
$ rustup default nightly-2016-11-25
$ cargo install cargo-doc-coverage
$ cd project/you/want/to/evaluate
$ cargo doc-coverage
```

If you don't see any output, try doing a `cargo clean` or making a change in
your code to trigger a recompile.

What you should see:

```
$ cargo doc-coverage
   Compiling nom v2.0.1 (file:///Users/carolnichols/rust/nom)
warning: 225 items, 117 undocumented, 48% documented

    Finished debug [unoptimized + debuginfo] target(s) in 0.77 secs
```

## Inspiration/where most of this code came from

A large part of this code is from [clippy][] and the [rustc missing-docs lint][].

[rustup]: https://rustup.rs/
[clippy]: https://crates.io/crates/clippy
[rustc missing-docs lint]: https://github.com/rust-lang/rust/blob/bc096549e84d1edbd051f0b42662c038dd935ff6/src/librustc_lint/builtin.rs#L256-L482
